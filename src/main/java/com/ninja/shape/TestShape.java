/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.shape;

/**
 *
 * @author USER
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle = new Circle(3);
        circle.print();
        Circle circle1 = new Circle(4);
        circle1.print();
        
        Rectangle rec = new Rectangle(4,3);
        rec.print();
        
        Triangle triangle = new Triangle(4,3);
        triangle.print();
        
        Square square = new Square(2);
        square.print();
        
        Shape[] shape = {circle, circle1, rec, triangle, square};
        for(int i = 0; i < shape.length; i++){
            shape[i].print();
        }
    }
}

