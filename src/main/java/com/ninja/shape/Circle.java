/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.shape;

/**
 *
 * @author USER
 */
public class Circle extends Shape{

    private double r;
    private double pi = 22.0 / 7;

    public Circle(double r) {
        if(r <= 0){
            System.out.println("Error: Radius must more than zero!!!!");
            
         }
        this.r = r;
     }
    @Override
    public double calArea(){
        return pi * r * r;
        
    }
    
    @Override
    public void print() {
        System.out.println("Circle radius : " + this.r + " Area is = " + calArea());
                
    }

}
